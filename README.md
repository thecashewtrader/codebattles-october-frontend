# Introduction

This is my submission for the Frontend part of the [CodeBattles October Challenge](https://codebattles.dev/event/dce4b8cd-b48d-4511-b4d6-b0058c179944/).

It is hosted at [Netlify](https://codebattles-october-frontend.netlify.app)
