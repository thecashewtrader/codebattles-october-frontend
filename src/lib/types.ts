export interface Company {
	name: string;
	username: string;
	logo: string;
	bio: string;
	twitter: string | null;
	linkedin: string | null;
	advocates: number[];
}

export interface Advocate {
	name: string;
	username: string;
	profile_pic: string;
	bio: string;
	twitter: string | null;
	follower_count: number;
	companies: number[];
}

export interface Pagination {
	current_page: number;
	total_pages: number;
	pages: number[];
	has_previous: boolean;
	has_next: boolean;
	prev_page: boolean;
	next_page: boolean;
	results_found: number;
}
