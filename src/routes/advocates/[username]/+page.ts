import type { LoadEvent } from '@sveltejs/kit';
import { baseURI } from '$lib/const';
import type { Advocate, Company } from '$lib/types';

export const load = async ({ params, fetch }: LoadEvent) => {
	const res = await fetch(`${baseURI}/advocates/${params.username}`);
	const json = await res.json();
	const advocate: Advocate = json['advocate'];

	return { advocate };
};
