import type { LoadEvent } from '@sveltejs/kit';
import { baseURI } from '$lib/const';
import type { Advocate, Pagination } from '$lib/types';

export const load = async ({ url, fetch }: LoadEvent) => {
	let fetchURL = new URL(`${baseURI}/advocates`);

	const query = url.searchParams.get('query');
	const page = url.searchParams.get('page');

	if (query && query != '') {
		fetchURL.searchParams.append('query', query);
	}
	if (page && page != '') {
		fetchURL.searchParams.append('page', page);
	}

	const res = await fetch(fetchURL);
	const json = await res.json();

	const advocates: Advocate[] = json['advocates'];
	const pagination: Pagination = json['pagination'];
	return { advocates, pagination };
};
