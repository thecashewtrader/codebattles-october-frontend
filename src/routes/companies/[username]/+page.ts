import type { LoadEvent } from '@sveltejs/kit';
import { baseURI } from '$lib/const';
import type { Company } from '$lib/types';

export const load = async ({ params, fetch }: LoadEvent) => {
	const res = await fetch(`${baseURI}/companies/${params.username}`);
	const json = await res.json();
	const company: Company = json['company'];
	return { company };
};
