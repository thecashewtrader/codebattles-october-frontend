import type { LoadEvent } from '@sveltejs/kit';
import { baseURI } from '$lib/const';
import type { Company } from '$lib/types';

export const load = async ({ fetch }: LoadEvent) => {
	let fetchURL = `${baseURI}/companies`;

	const res = await fetch(fetchURL);
	const json = await res.json();

	const companies: Company[] = json['companies'];

	return { companies };
};
