import { test, expect } from '@playwright/test';

test('Advocates', async ({ page }) => {
	await page.goto('/');
	await page.locator('text = Advocates').click();
	await page.waitForURL('/advocates');
	await page.waitForTimeout(500);

	await expect(page).toHaveURL('/advocates');
	await expect(await page.textContent('h1')).toBe('Advocates');

	await page.locator('text = Dennis Ivy').click();
	await page.waitForURL('/advocates/1');
	await page.waitForTimeout(500);

	await expect(page).toHaveURL('/advocates/1');
	await expect(await page.textContent('h1')).toBe('Dennis Ivy');
	await expect(await page.textContent('h2')).toBe('About Dennis Ivy');
});

test('Advocates Query', async ({ page }) => {
	await page.goto('/');
	await page.locator('text = Advocates').click();
	await page.waitForURL('/advocates');
	await page.waitForTimeout(500);
	await expect(page).toHaveURL('/advocates');

	await page.getByPlaceholder('Search...').click();
	await page.getByPlaceholder('Search...').fill('Tadas');
	await page.getByRole('button', { name: 'Search' }).click();
	await page.waitForURL('/advocates?query=Tadas');
	await page.waitForTimeout(500);

	await expect(page).toHaveURL('/advocates?query=Tadas');

	await expect(page.locator('css=.hero.bg-primary')).toContainText('Tadas Petra');
	await expect(page.locator('css=.hero.bg-primary')).not.toContainText('Dennis Ivy');
});
