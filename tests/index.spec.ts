import { test, expect } from '@playwright/test';

test('Index', async ({ page }) => {
	await page.goto('/');
	await expect(await page.textContent('h1')).toBe('CodeBattles October Frontend');
	await expect(await page.textContent('h2')).toBe('Technical Details');
	await page.locator('input').first().check();
	await page.locator('input').first().uncheck();
});
