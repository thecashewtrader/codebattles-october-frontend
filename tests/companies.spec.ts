import { test, expect } from '@playwright/test';

test('Companies', async ({ page }) => {
	await page.goto('/');

	await page.locator('text = Companies').click();
	await page.waitForURL('/companies');
	await page.waitForTimeout(500);

	await expect(page).toHaveURL('/companies');
	await expect(await page.textContent('h1')).toBe('Companies');

	await page.locator('text = Agora').click();
	await page.waitForURL('/companies/1');
	await page.waitForTimeout(500);

	await expect(page).toHaveURL('/companies/1');
	await expect(await page.textContent('h1')).toBe('Agora');
});
