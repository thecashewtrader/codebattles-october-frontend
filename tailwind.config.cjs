const daisyui = require('daisyui');
const typography = require('@tailwindcss/typography');

const config = {
	content: ['./src/**/*.{html,js,svelte,ts}'],

	theme: {
		fontFamily: {
			sans: ['Inter', 'sans-serif']
		}
	},

	plugins: [typography, daisyui],
	daisyui: {
		styled: true,
		themes: ['night']
	}
};

module.exports = config;
